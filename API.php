<?php

class API
{
    public function readEmployers($id)
    {
        $curl = curl_init();
        if ($id == NULL) {
            $url = "https://dummy.restapiexample.com/api/v1/employees"; 
        } else {
            $url = "https://dummy.restapiexample.com/api/v1/employee/" . $id;
        }
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ]);

        $data = curl_exec($curl);
        if ($error = curl_error($curl)) {
            return $error;
        } else return $data;
        curl_close($curl);
    }

    public function createEmployee($name, $salary, $age, $image)
    {
        $curl = curl_init();
        $url = "https://dummy.restapiexample.com/api/v1/create";
        $data_array = array(
            'id' => '',
            "employee_name" => $name,
            "employee_salary" => $salary,
            "employee_age" => $age,
            'profile_image' => $image,
        );

        $data = http_build_query($data_array);

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_POST => true, 
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true
        ]);

        $response = curl_exec($curl);
        if ($error = curl_error($curl)) {
            return $error;
        } else return $response;
        curl_close($curl);
    }
}

$class = new API();
print_r($class -> readEmployers(NULL));

$decoded = json_decode($class -> createEmployee('insdds quibusdam temp', 2000, 12, ""));
print_r($decoded);